<?php

namespace app\models;
use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 *
 * @property Lead $lead
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'leadId', 'name', 'amount'], 'required'],
            [['id', 'leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['leadId'], 'exist', 'skipOnError' => true, 'targetClass' => Lead::className(), 'targetAttribute' => ['leadId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLead()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadId']);
    }
    public function getDealItem()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadId']);
    }
}
