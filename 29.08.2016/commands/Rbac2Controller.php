<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{
	
	public function actionTmpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$indexDeals = $auth->createPermission('indexDeals');
		$indexDeals->description = 'All users can view deals';
		$auth->add($indexDeals);
		
		$updateOwnDeal = $auth->createPermission('updateOwnDeal');
		$updateOwnDeal->description = 'Team member can update
									only his/her own deals';
		$auth->add($updateOwnDeal);

		$viewDeal = $auth->createPermission('viewDeal');
		$viewDeal->description = 'View deals';
		$auth->add($viewDeal);

			
	}


	public function actionTlpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createDeal = $auth->createPermission('createDeal');
		$createDeal->description = 'Team leader can create new deals';
		$auth->add($createDeal);
		
		$updateDeal = $auth->createPermission('updateDeal');
		$updateDeal->description = 'Team leader can update
									deals including assignment';
		$auth->add($updateDeal);		
	}
	



	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$teammember = $auth->getRole('teammember');

		$indexDeals = $auth->getPermission('indexDeals');
		$auth->addChild($teammember, $indexDeals);

		$updateOwnDeal = $auth->getPermission('updateOwnDeal');
		$auth->addChild($teammember, $updateOwnDeal);

		$viewDeal = $auth->getPermission('viewDeal');
		$auth->addChild($teammember, $viewDeal);		
		
		$teamleader = $auth->getRole('teamleader');
		$auth->addChild($teamleader, $teammember);
		
		$createDeal = $auth->getPermission('createDeal');
		$auth->addChild($teamleader, $createDeal);

		$updateDeal = $auth->getPermission('updateDeal');
		$auth->addChild($teamleader, $updateDeal);
		
		
	}
}